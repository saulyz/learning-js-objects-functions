var Universe;

(function() {
    
    var instance;
    // can be some more private vars or functions

    Universe = function Universe() {
        if (instance) {
            return instance;
        }

        instance = this;

        // all the functionality
        this.start_time = 0,
        this.bang = 'Big',
        this.start = function() {
            console.log(this.bang + '!!!');
        }
    }
})();

var uni1 = new Universe();
var uni2 = new Universe();

console.log('checking if both uni are equal --> ', uni1 === uni2);
console.log(uni1);
console.log(uni2);