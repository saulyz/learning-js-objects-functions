var Person1 = function(name) {

    // create a new object using object literal
    // var this = {}

    // add properties and methods
    this.name = name;
    this.say = function() {
        return 'Hi, I am ' + this.name;
    }

    // return this;
}


// using prototype
var Person2 = function(name) {
    this.name = name;
}
Person2.prototype.say = function() {
    return 'Hey, I am ' + this.name + '. And I\'m more efficient version';
}


var me = new Person1('Saulius');
console.log(me.say());

var notMe = new Person2('Paulius');
console.log(notMe.say());
